package com.yopaisa.Service;



import com.yopaisa.Model.RestResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by RakeshD on 1/3/2018.
 */

public class LoginService {
    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/users/";

    public interface LoginAPI {
        @POST("login")
        @FormUrlEncoded
        Call<RestResponse> getResults(@Field("email") String email,
                                      @Field("password") String password);
    }

    public LoginService.LoginAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(LoginAPI.class);
    }
}
