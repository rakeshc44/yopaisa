package com.yopaisa.Service;

import com.yopaisa.Model.RestResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by rakesh on 8/13/2018.
 * Project K.
 */

public class NotificationService {
    private static String BASE_URL = "http://kabhadi.com/apis/";

    public interface NotificationListAPI {
        @GET("ratelist")
        Call<RestResponse> getResults();
    }

    public NotificationService.NotificationListAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(NotificationService.NotificationListAPI.class);
    }
}
