package com.yopaisa.Service;



import com.yopaisa.data.Prefs.AppPreference;
import com.yopaisa.response.ContactUsResponse;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rakesh on 5/30/2018.
 */

public class ContactUsService {
    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/contactus/";

    public interface ContactUsAPI {
        @POST("add")
        @FormUrlEncoded
        Call<ContactUsResponse> getResults(@Field("name") String name,
                                           @Field("email") String email,
                                           @Field("phone") String phone,
                                           @Field("message") String message);
    }

    public ContactUsService.ContactUsAPI getAPI() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Token", AppPreference.getInstance().getmUserAccessToken());
                        return chain.proceed(ongoing.build());
                    }
                })
                .build();
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        return retrofit.create(ContactUsService.ContactUsAPI.class);
    }
}
