package com.yopaisa.Service;



import com.yopaisa.Model.RestResponse;
import com.yopaisa.data.Prefs.AppPreference;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;

/**
 * Created by rakesh on 6/5/2018.
 */

public class ProfileService {
    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/users/";

    public interface GetProfiletAPI {
        @POST("getProfile")
        Call<RestResponse> getResults();
    }

    public GetProfiletAPI getAPI() {

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Token", AppPreference.getInstance().getmUserAccessToken());
                        return chain.proceed(ongoing.build());
                    }
                })
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        return retrofit.create(GetProfiletAPI.class);
    }
}
