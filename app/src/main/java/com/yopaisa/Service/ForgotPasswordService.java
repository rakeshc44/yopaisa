package com.yopaisa.Service;



import com.yopaisa.response.ForgotPasswordResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rakesh on 6/19/2018.
 * Project K.
 */

public class ForgotPasswordService {
    private static String BASE_URL = "http://kabhadi.com/index.php/";

    public interface ForgotPasswordAPI {
        @POST("users/forget_password")
        @FormUrlEncoded
        Call<ForgotPasswordResponse> getResults(@Field("email") String email);
    }

    public ForgotPasswordService.ForgotPasswordAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(ForgotPasswordService.ForgotPasswordAPI.class);
    }
}
