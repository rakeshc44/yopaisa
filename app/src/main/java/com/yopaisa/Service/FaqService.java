package com.yopaisa.Service;


import com.yopaisa.Model.RestResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by rakesh on 6/5/2018.
 */

public class FaqService {
    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/faq/";

    public interface FaqListAPI {
        @GET("get_faqs")
        Call<RestResponse> getResults();
    }

    public FaqService.FaqListAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(FaqService.FaqListAPI.class);
    }
}
