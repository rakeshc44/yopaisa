package com.yopaisa.Service;



import com.yopaisa.Model.RestResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by RakeshD on 1/3/2018.
 */

public class SignUpService {
    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/users/";

    public interface SignUpAPI {
        @POST("add")
        @FormUrlEncoded
        Call<RestResponse> getResults(@Field("name") String name,
                                      @Field("email") String email,
                                      @Field("password") String password,
                                      @Field("phone") String phone,
                                      @Field("address") String address,
                                      @Field("type") String type,
                                      @Field("city") String city,
                                      @Field("zip_code") String zipCode,
                                      @Field("refral_code") String referalCode
        );
    }

    public SignUpService.SignUpAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(SignUpService.SignUpAPI.class);
    }
}
