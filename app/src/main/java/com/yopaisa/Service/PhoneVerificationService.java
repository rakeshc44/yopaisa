package com.yopaisa.Service;

import com.yopaisa.Model.RestResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rakesh on 8/13/2018.
 * Project K.
 */

public class PhoneVerificationService {
    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/users/";

    public interface OtpVerifyAPI {
        @POST("varifyOtp")
        @FormUrlEncoded
        Call<RestResponse> getResults(
                                      @Field("otp") String otpCode,
                                        @Field("email") String email
        );
    }

    public PhoneVerificationService.OtpVerifyAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(PhoneVerificationService.OtpVerifyAPI.class);
    }
}
