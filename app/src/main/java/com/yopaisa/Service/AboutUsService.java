package com.yopaisa.Service;



import com.yopaisa.Model.RestResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by RakeshD on 1/22/2018.
 */

public class AboutUsService {

    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/pages/";

    public interface AboutUsAPI {
        @POST("get_page")
        @FormUrlEncoded
        Call<RestResponse> getResults(@Field("page_url") String email);
    }

    public AboutUsService.AboutUsAPI getAPI() {
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(AboutUsService.AboutUsAPI.class);
    }
}
