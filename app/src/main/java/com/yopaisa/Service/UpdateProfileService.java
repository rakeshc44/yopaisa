package com.yopaisa.Service;


import com.yopaisa.Model.RestResponse;
import com.yopaisa.data.Prefs.AppPreference;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by rakesh on 6/21/2018.
 * Project K.
 */

public class UpdateProfileService {
    private static String BASE_URL = "http://oneadd.kabhadi.com/apis/index.php/users/";

    public interface UpdateProfileAPI {
        @POST("update_profile")
        @FormUrlEncoded
        Call<RestResponse> getResults(@Field("name") String name,
                                      @Field("email") String email,
                                      @Field("phone") String phone,
                                      @Field("city") String city,
                                      @Field("zip_code") String pincode,
                                      @Field("gender") String gender,
                                      @Field("marital_status") String marital_status,
                                      @Field("have_child") String have_child,
                                      @Field("profession") String profession,
                                      @Field("annual_family_income") String annual_family_income,
                                      @Field("most_active_facebook") String most_active_facebook,
                                      @Field("most_active_instagram") String most_active_instagram,
                                      @Field("most_active_youtube") String most_active_youtube,
                                      @Field("most_active_whatsapp") String most_active_whatsapp,
                                      @Field("most_active_travel_site") String most_active_travel_site,
                                      @Field("most_active_restaurant_site") String most_active_restaurant_site,
                                      @Field("password") String password,
                                      @Field("address") String address

        );
    }

    public UpdateProfileAPI getAPI() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Token", AppPreference.getInstance().getmUserAccessToken());
                        return chain.proceed(ongoing.build());
                    }
                })
                .build();
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();
        return retrofit.create(UpdateProfileAPI.class);
    }
}
