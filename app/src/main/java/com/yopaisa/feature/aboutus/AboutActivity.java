package com.yopaisa.feature.aboutus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.yopaisa.Model.Data;
import com.yopaisa.Presenter.AboutUsPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.DeviceUtils;

import java.util.List;

/**
 * Created by RakeshD on 1/22/2018.
 */

public class AboutActivity extends AppCompatActivity implements AboutUsPresenter.AboutUsPresenterListener {

    private AboutUsPresenter aboutUsPresenter;
    private TextView mTxtAboutUs;
    private LinearLayout mRlAboutUs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        mRlAboutUs = findViewById(R.id.about_Lnr);
        mTxtAboutUs = findViewById(R.id.about_us_TxtAboutUs);


        aboutUsPresenter = new AboutUsPresenter(this, this);
        if (DeviceUtils.isInternetOn1(AboutActivity.this, mRlAboutUs)) {
            aboutUsPresenter.getAboutUs("about-us");
        }

    }

    @Override
    public void aboutUsReady(List<Data> users) {



        mTxtAboutUs.setText(users.get(0).getDescription());
    }


}
