package com.yopaisa.feature.termcondition;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;

import com.yopaisa.Model.RestResponse;
import com.yopaisa.Presenter.TermConditionPresenter;
import com.yopaisa.R;


/**
 * Created by RakeshD on 1/22/2018.
 */

public class TermConditionActivity extends AppCompatActivity implements TermConditionPresenter.TermConditionPresenterListener {

    private TermConditionPresenter termConditionPresenter;
    private TextView mTxtTermCondition;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);

        mTxtTermCondition = findViewById(R.id.terms_condition_TxtDescription);


        termConditionPresenter = new TermConditionPresenter(this, this);
        if(getIntent().getStringExtra("TERMSANDCONDITION").equalsIgnoreCase("TERMSANDCONDITION")) {
            termConditionPresenter.getTermCondition("terms-and-conditions");
        }
        else{
            termConditionPresenter.getTermCondition("privacy-policy");
        }


    }


    @Override
    public void termConditionReady(RestResponse response) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mTxtTermCondition.setText(Html.fromHtml(response.getData().get(0).getDescription(), Html.FROM_HTML_MODE_COMPACT));
        }
        else{
            mTxtTermCondition.setText((Html.fromHtml(response.getData().get(0).getDescription())));
        }
    }
}
