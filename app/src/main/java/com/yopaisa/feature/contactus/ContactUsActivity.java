package com.yopaisa.feature.contactus;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.yopaisa.Presenter.ContactUsPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.AlertUtils;
import com.yopaisa.Utils.DeviceUtils;
import com.yopaisa.response.ContactUsResponse;


/**
 * Created by RakeshD on 1/22/2018.
 */

public class ContactUsActivity extends AppCompatActivity implements ContactUsPresenter.ContactusPresenterListener {
    private ContactUsPresenter contactUsPresenter;
    private EditText mEdtName, mEdtEmail, mEdtPhone, mEdtMessage;
    private Button mBtnSend;
    private String mName, mEmail, mPhone, mMessage;
    private RelativeLayout mRlContatcUs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        mEdtName = findViewById(R.id.login_EdtName);
        mEdtEmail = findViewById(R.id.login_EdtEmail);
        mEdtPhone = findViewById(R.id.login_EdtPhone);
        mEdtMessage = findViewById(R.id.login_EdtMessage);
        mBtnSend = findViewById(R.id.login_BtnSend);
        mRlContatcUs = findViewById(R.id.contactUs_rl);


        contactUsPresenter = new ContactUsPresenter(this, this);

        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validateName()) {
                    return;
                }
                if (!validateEmail()) {
                    return;
                }
                if (!validatePhone()) {
                    return;
                }
                if (!validateMessage()) {
                    return;
                } else if (DeviceUtils.isInternetOn1(ContactUsActivity.this, mRlContatcUs)) {
                    mName = mEdtName.getText().toString();
                    mEmail = mEdtEmail.getText().toString();
                    mPhone = mEdtPhone.getText().toString();
                    mMessage = mEdtMessage.getText().toString();
                    contactUsPresenter.getContactUs(mName, mEmail, mPhone, mMessage);
                }

            }
        });

    }


    @Override
    public void ContactUsReady(ContactUsResponse response) {
        if (response.getStatus().equalsIgnoreCase("true")) {
            AlertUtils.showSnackBarGreen(mRlContatcUs, response.getMessage());
        } else {
            AlertUtils.showSnackBar(mRlContatcUs, response.getErrors().getMessage());
        }

        Toast.makeText(ContactUsActivity.this, mMessage, Toast.LENGTH_SHORT).show();
    }

    private boolean validateName() {
        String name = mEdtName.getText().toString().trim();

        if (name.isEmpty() || !isValidName(name)) {
            AlertUtils.showSnackBar(mRlContatcUs, "Please Enter Valid Name");
            requestFocus(mEdtName);
            return false;
        } else {

        }
        return true;
    }

    private boolean validateEmail() {
        String email = mEdtEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            AlertUtils.showSnackBar(mRlContatcUs, "Please Enter Valid Email Address ");
            requestFocus(mEdtEmail);
            return false;
        } else {

        }
        return true;
    }

    private boolean validatePhone() {
        String phone = mEdtPhone.getText().toString().trim();

        if (phone.isEmpty() || !isValidPhone(phone)) {
            AlertUtils.showSnackBar(mRlContatcUs, "Please Enter Valid Phone Number");
            requestFocus(mEdtPhone);
            return false;
        } else {

        }
        return true;
    }

    private boolean validateMessage() {
        String message = mEdtMessage.getText().toString().trim();

        if (message.isEmpty()) {
            AlertUtils.showSnackBar(mRlContatcUs, "Please Enter Message");
            requestFocus(mEdtMessage);
            return false;
        } else {

        }
        return true;
    }

    private static boolean isValidName(String name) {
        return !TextUtils.isEmpty(name) && (name.length() > 4);
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isValidPhone(String phone) {
        return !TextUtils.isEmpty(phone) && (phone.length() > 1) && (phone.length() <= 10);
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
