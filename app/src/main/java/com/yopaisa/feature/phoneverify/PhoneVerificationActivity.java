package com.yopaisa.feature.phoneverify;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.yopaisa.MainActivity;
import com.yopaisa.Model.Data;
import com.yopaisa.Model.RestResponse;
import com.yopaisa.Presenter.AboutUsPresenter;
import com.yopaisa.Presenter.PhoneVerificationPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.AlertUtils;
import com.yopaisa.Utils.DeviceUtils;
import com.yopaisa.data.Prefs.AppPreference;
import com.yopaisa.feature.aboutus.AboutActivity;
import com.yopaisa.feature.login.LoginActivity;
import com.yopaisa.feature.signup.SignupActivity;

import java.util.List;

/**
 * Created by rakesh on 8/10/2018.
 * Project K.
 */

public class PhoneVerificationActivity extends AppCompatActivity implements PhoneVerificationPresenter.PhoneVerificationPresenterListener {
    private PhoneVerificationPresenter phoneVerificationPresenter;
    private RelativeLayout mRlPhoneVerification;
    private TextInputEditText mEdtOtpCode;
    private Button mBtnVerify;
    private String mOtpCode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        mRlPhoneVerification = findViewById(R.id.phone_verification_rl);
        mEdtOtpCode = findViewById(R.id.phone_verification_EdtOTP);
        mBtnVerify = findViewById(R.id.phone_verification_btnVerify);
        phoneVerificationPresenter = new PhoneVerificationPresenter(this, this);
        System.out.println(">>>>" + "success");
        mBtnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validateOTP()) {
                    return;
                } else if (DeviceUtils.isInternetOn1(PhoneVerificationActivity.this, mRlPhoneVerification)) {

                    mOtpCode = mEdtOtpCode.getText().toString();

                    phoneVerificationPresenter.getOtpVerify(mOtpCode);
                }

            }
        });

    }

    private boolean validateOTP() {
        String password = mEdtOtpCode.getText().toString().trim();

        if (password.isEmpty() || !isValidPassword(password)) {
            AlertUtils.showSnackBar(mRlPhoneVerification, "Please Enter Valid OTP");
            requestFocus(mEdtOtpCode);
            return false;
        } else {

        }
        return true;
    }

    private static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 1;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void otpVerifyReady(RestResponse response) {

        System.out.println(">>>>" + "success");
        if (response.getStatus().equalsIgnoreCase("true")) {
            System.out.println(">>>>" + "success1");
            AppPreference.getInstance().setmIsPhoneVerified(response.getData().get(0).getIs_varified());
            if(AppPreference.getInstance().getmUserAccessToken().length() >1){
                Intent intent = new Intent(PhoneVerificationActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
            else {
                Intent intent = new Intent(PhoneVerificationActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            AlertUtils.showSnackBar(mRlPhoneVerification, response.getMessage());
        }

    }
}
