package com.yopaisa.feature.faq;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.yopaisa.Model.Data;
import com.yopaisa.R;

import java.util.List;

/**
 * Created by rakesh on 6/5/2018.
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.MyViewHolder> {
    private List<Data> rateList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtName, mTxtRate;

        public MyViewHolder(View view) {
            super(view);
            mTxtName = view.findViewById(R.id.item_list_TxtTitle);
            mTxtRate = view.findViewById(R.id.item_list_TxtRate);

        }
    }


    public FaqAdapter(List<Data> rateList) {
        this.rateList = rateList;
    }

    @Override
    public FaqAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_faq, parent, false);

        return new FaqAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FaqAdapter.MyViewHolder holder, int position) {
        Data rateLst = rateList.get(position);
        holder.mTxtName.setText(rateLst.getQuestion());
        holder.mTxtRate.setText(rateLst.getAnswer());

    }

    @Override
    public int getItemCount() {
        return rateList.size();
    }
}

