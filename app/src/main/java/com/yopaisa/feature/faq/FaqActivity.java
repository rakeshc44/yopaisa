package com.yopaisa.feature.faq;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;


import com.yopaisa.Model.Data;
import com.yopaisa.Presenter.FaqPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.DeviceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rakesh on 6/5/2018.
 */

public class FaqActivity extends AppCompatActivity implements FaqPresenter.FaqListPresenterListener {

    private FaqPresenter faqPresenter;
    private List<Data> faqList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FaqAdapter faqAdapter;
    private RelativeLayout mRlRateList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_list);

        mRlRateList = findViewById(R.id.ratelist_Rl);
        recyclerView = findViewById(R.id.recycler_view);
        faqPresenter = new FaqPresenter(this, this);
        if (DeviceUtils.isInternetOn1(FaqActivity.this, mRlRateList)) {
            faqPresenter.getFaqList();
        }

    }

    @Override
    public void faqListReady(List<Data> users) {

        faqAdapter = new FaqAdapter(users);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(faqAdapter);
        faqAdapter.notifyDataSetChanged();

    }
}

