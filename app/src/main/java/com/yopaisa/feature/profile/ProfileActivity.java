package com.yopaisa.feature.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.yopaisa.MainActivity;
import com.yopaisa.Model.RestResponse;
import com.yopaisa.Presenter.ProfilePresenter;
import com.yopaisa.Presenter.UpdateProfilePresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.AlertUtils;
import com.yopaisa.Utils.DeviceUtils;
import com.yopaisa.data.Prefs.AppPreference;
import com.yopaisa.feature.phoneverify.PhoneVerificationActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rakesh on 8/16/2018.
 * Project K.
 */

public class ProfileActivity extends AppCompatActivity implements ProfilePresenter.ProfilePresenterListener, UpdateProfilePresenter.UpdateProfilePresenterListener, AdapterView.OnItemSelectedListener {

    private ProfilePresenter profilePresenter;
    private UpdateProfilePresenter updateProfilePresenter;
    private EditText mEdtName, mEdtEmail, mEdtPhone, mEdtCity, mEdtPinCode;
    private RadioGroup mRgGender, mRgMaritialStatus, mRgHaveChild, mRgAnnualIncome,
            mRgFacebook, mRgYoutube, mRgInstagram, mRgWhatsapp, mRgTravel, mRgRestaurant;
    private RadioButton mRbMale, mRbFemale, mRbYes, mRbNo, mRbLesFive, mRbLessTen,
            mRblessTwenty, mRbLessFifity, mRbMoreFifty,
            mRbFbSomeTime, mRbFbFrequent, mRbFbNotActive,
            mRbYtSomeTime, mRbYtFrequent, mRbYtNotActive,
            mRbIgSomeTime, mRbIgFrequent, mRbIgNotActive,
            mRbWaSomeTime, mRbWaFrequent, mRbWaNotActive,
            mRbTrSomeTime, mRbTrFrequent, mRbTrNotActive, mRbRsSomeTime, mRbRsFrequent, mRbRsNotActive;
    private Spinner mSpnrProfession;
    private Button mBtnDob, mBtnSave;

    private TextView mTxtHaveChild;
    private LinearLayout mLnrHaveChild;

    private String mName, mEmail, mphone, mCity, mPinCode, mSex, mMarital, mIsChild,
            mFacebook, mInstagram, mYoutube, mWhatsapp, mTravelSite, mRestaurant;
    private RelativeLayout mRlProfile;
    private RadioButton mRbGender, mRbMaritalStatus, mRbHaveChild, mRbAnnualIncome;

    private String mProfession;
    private RadioButton mRbFacebook;
    private RadioButton mRbYoutube;
    private RadioButton mRbInstagram;
    private RadioButton mRbWhatsApp;
    private RadioButton mRbTravelSite;
    private RadioButton mRbRestaurant;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mEdtName = findViewById(R.id.profile_EdtName);
        mEdtEmail = findViewById(R.id.profile_EdtEmail);
        mEdtPhone = findViewById(R.id.profile_EdtPhone);

        mEdtCity = findViewById(R.id.profile_EdtCity);
        mEdtPinCode = findViewById(R.id.profile_EdtPinCode);

        mLnrHaveChild = findViewById(R.id.profile_lnrhaveChild);
        mRlProfile = findViewById(R.id.profile_rl);
        mTxtHaveChild = findViewById(R.id.profile_txtHaveChild);

        mRgGender = findViewById(R.id.profile_rgGender);
        mRgMaritialStatus = findViewById(R.id.profile_rgMaritialStatus);
        mRgHaveChild = findViewById(R.id.profile_rgHaveChild);
        mRgAnnualIncome = findViewById(R.id.profile_rgFamilyIncome);
        mRgFacebook = findViewById(R.id.profile_rgfacebook);
        mRgYoutube = findViewById(R.id.profile_rgYoutube);
        mRgInstagram = findViewById(R.id.profile_rgInstagram);
        mRgWhatsapp = findViewById(R.id.profile_rgWhatsapp);
        mRgTravel = findViewById(R.id.profile_rgTravelSites);
        mRgRestaurant = findViewById(R.id.profile_rgRestaurantSites);


        /*mRbMale = findViewById(R.id.profile_rbMale);
        mRbFemale = findViewById(R.id.profile_rbFeMale);
        mRbYes = findViewById(R.id.profile_rbYes);
        mRbNo = findViewById(R.id.profile_rbNo);
        mRbLesFive = findViewById(R.id.profile_rbLessFive);
        mRbLessTen = findViewById(R.id.profile_rbLessTen);
        mRblessTwenty = findViewById(R.id.profile_rbLessTwenty);
        mRbLessFifity = findViewById(R.id.profile_rbLessFifty);
        mRbMoreFifty = findViewById(R.id.profile_rbGreaterFifty);

        mRbFbSomeTime = findViewById(R.id.profile_rbFbSomeTime);
        mRbFbFrequent = findViewById(R.id.profile_rbFbFrequently);
        mRbFbNotActive = findViewById(R.id.profile_rbFbNotActive);

        mRbIgSomeTime = findViewById(R.id.profile_rbIgSomeTime);
        mRbIgFrequent = findViewById(R.id.profile_rbIgFrequently);
        mRbIgNotActive = findViewById(R.id.profile_rbIgNotActive);

        mRbYtSomeTime = findViewById(R.id.profile_rbYtSomeTime);
        mRbYtFrequent = findViewById(R.id.profile_rbYtFrequently);
        mRbYtNotActive = findViewById(R.id.profile_rbYtNotActive);

        mRbWaSomeTime = findViewById(R.id.profile_rbWaSomeTime);
        mRbWaFrequent = findViewById(R.id.profile_rbWaFrequently);
        mRbWaNotActive = findViewById(R.id.profile_rbWaNotActive);

        mRbTrSomeTime = findViewById(R.id.profile_rbTsSomeTime);
        mRbTrFrequent = findViewById(R.id.profile_rbTsFrequently);
        mRbTrNotActive = findViewById(R.id.profile_rbTsNotActive);

        mRbRsSomeTime = findViewById(R.id.profile_rbRsSomeTime);
        mRbRsFrequent = findViewById(R.id.profile_rbRsFrequently);
        mRbRsNotActive = findViewById(R.id.profile_rbRsNotActive);*/


        mBtnSave = findViewById(R.id.profile_BtnSave);

        mSpnrProfession = findViewById(R.id.profile_spnrProfession);
        mSpnrProfession.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mSpnrProfession.setAdapter(dataAdapter);

        profilePresenter = new ProfilePresenter(this, this);

        if (DeviceUtils.isInternetOn1(ProfileActivity.this, mRlProfile)) {
            profilePresenter.getProfile();
        }
        updateProfilePresenter = new UpdateProfilePresenter(this, this);

       /* mBtnUpdatePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               *//* PasswordDialog passwordDialog = new PasswordDialog(ProfileActivity.this, mRlProfile);
                passwordDialog.show();*//*
            }
        });*/

        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateName()) {
                    return;
                }
                if (!validateEmail()) {
                    return;
                }
                if (!validatePhone()) {
                    return;
                }
               /* if (!validateAddress()) {
                    return;
                }
                if (!validatePassword()) {
                    return;
                }*/
                if (DeviceUtils.isInternetOn1(ProfileActivity.this, mRlProfile)) {

                    mName = mEdtName.getText().toString();
                    mEmail = mEdtEmail.getText().toString();
                    mphone = mEdtPhone.getText().toString();
                    mCity = mEdtCity.getText().toString();
                    mPinCode = mEdtPinCode.getText().toString();

                   /* mAddress = mEdtAddress.getText().toString();
                    mPassword = mEdtPassword.getText().toString();*/


                    int mGender = mRgGender.getCheckedRadioButtonId();
                    mRbGender = (RadioButton) findViewById(mGender);
                    if(mRbGender.getText().toString().equalsIgnoreCase("Male")){
                        mSex = "1";
                    }
                    else{
                        mSex = "2";
                    }

                    int mMaritalStatus = mRgMaritialStatus.getCheckedRadioButtonId();
                    mRbMaritalStatus = (RadioButton) findViewById(mMaritalStatus);
                    if( mRbMaritalStatus.getText().toString().equalsIgnoreCase("Single")){
                        mMarital = "1";
                    }
                    else{
                        mMarital = "2";
                    }
                    int mHaveChild = mRgHaveChild.getCheckedRadioButtonId();
                    mRbHaveChild = (RadioButton) findViewById(mHaveChild);

                    if( mRbHaveChild.getText().toString().equalsIgnoreCase("Yes")){
                        mIsChild = "1";
                    }
                    else{
                        mIsChild = "2";
                    }
                    int mAnnualIncome = mRgAnnualIncome.getCheckedRadioButtonId();
                    mRbAnnualIncome = (RadioButton) findViewById(mAnnualIncome);

                    int mFacebookCount = mRgFacebook.getCheckedRadioButtonId();
                    mRbFacebook = (RadioButton) findViewById(mFacebookCount);

                    if( mRbFacebook.getText().toString().equalsIgnoreCase("Some Time")){
                        mFacebook = "1";
                    }
                    else if( mRbFacebook.getText().toString().equalsIgnoreCase("Frequently")){
                        mFacebook = "2";
                    }
                    else if( mRbFacebook.getText().toString().equalsIgnoreCase("Not Active")){
                        mFacebook = "3";
                    }
                    int mYoutubeCount = mRgYoutube.getCheckedRadioButtonId();
                    mRbYoutube = (RadioButton) findViewById(mYoutubeCount);
                    if( mRbYoutube.getText().toString().equalsIgnoreCase("Some Time")){
                        mYoutube = "1";
                    }
                    else if( mRbYoutube.getText().toString().equalsIgnoreCase("Frequently")){
                        mYoutube = "2";
                    }
                    else if( mRbYoutube.getText().toString().equalsIgnoreCase("Not Active")){
                        mYoutube = "3";
                    }
                    int mInstagramCount = mRgInstagram.getCheckedRadioButtonId();
                    mRbInstagram = (RadioButton) findViewById(mInstagramCount);
                    if( mRbInstagram.getText().toString().equalsIgnoreCase("Some Time")){
                        mInstagram = "1";
                    }
                    else if( mRbInstagram.getText().toString().equalsIgnoreCase("Frequently")){
                        mInstagram = "2";
                    }
                    else if( mRbInstagram.getText().toString().equalsIgnoreCase("Not Active")){
                        mInstagram = "3";
                    }
                    int mWhatsappCount = mRgWhatsapp.getCheckedRadioButtonId();
                    mRbWhatsApp = (RadioButton) findViewById(mWhatsappCount);
                    if( mRbWhatsApp.getText().toString().equalsIgnoreCase("Some Time")){
                        mWhatsapp = "1";
                    }
                    else if( mRbWhatsApp.getText().toString().equalsIgnoreCase("Frequently")){
                        mWhatsapp = "2";
                    }
                    else if( mRbWhatsApp.getText().toString().equalsIgnoreCase("Not Active")){
                        mWhatsapp = "3";
                    }
                    int mTravelSiteCount = mRgTravel.getCheckedRadioButtonId();
                    mRbTravelSite = (RadioButton) findViewById(mTravelSiteCount);
                    if( mRbTravelSite.getText().toString().equalsIgnoreCase("Some Time")){
                        mTravelSite = "1";
                    }
                    else if( mRbTravelSite.getText().toString().equalsIgnoreCase("Frequently")){
                        mTravelSite = "2";
                    }
                    else if( mRbTravelSite.getText().toString().equalsIgnoreCase("Not Active")){
                        mTravelSite = "3";
                    }
                    int mRestaurantCount = mRgRestaurant.getCheckedRadioButtonId();
                    mRbRestaurant = (RadioButton) findViewById(mRestaurantCount);
                    if( mRbRestaurant.getText().toString().equalsIgnoreCase("Some Time")){
                        mRestaurant = "1";
                    }
                    else if( mRbRestaurant.getText().toString().equalsIgnoreCase("Frequently")){
                        mRestaurant = "2";
                    }
                    else if( mRbRestaurant.getText().toString().equalsIgnoreCase("Not Active")){
                        mRestaurant = "3";
                    }

                    updateProfilePresenter.getUpdateProfile(mName, mEmail, mphone, mCity, mPinCode, mSex,
                           mMarital,
                            mIsChild, mProfession, mRbAnnualIncome.getText().toString(),
                            mFacebook,
                            mInstagram, mYoutube, mWhatsapp,
                           mTravelSite,mRestaurant);
                }
            }
        });

    }

    @Override
    public void profileReady(RestResponse response) {


        if (response.getStatus().equalsIgnoreCase("true")) {
            mEdtName.setText(response.getData().get(0).getName());
            mEdtEmail.setText(response.getData().get(0).getEmail());
            mEdtPhone.setText(response.getData().get(0).getPhone());
            mEdtCity.setText(response.getData().get(0).getCity());
            mEdtPinCode.setText(response.getData().get(0).getZip_code());

            String myString = response.getData().get(0).getProfession(); //the value you want the position for

            ArrayAdapter myAdap = (ArrayAdapter) mSpnrProfession.getAdapter(); //cast to an ArrayAdapter

            int spinnerPosition = myAdap.getPosition(myString);

//set the default according to value
            mSpnrProfession.setSelection(spinnerPosition);

            if (response.getData().get(0).getGender() != null) {
                if (response.getData().get(0).getGender().equalsIgnoreCase("1")) {
                    mRgGender.check(R.id.profile_rbMale);
                } else {
                    mRgGender.check(R.id.profile_rbFeMale);
                }
            }


            if (response.getData().get(0).getMarital_status() != null) {
                if (response.getData().get(0).getMarital_status().equalsIgnoreCase("1")) {
                    mRgMaritialStatus.check(R.id.profile_rbSingle);
                } else {
                    mRgMaritialStatus.check(R.id.profile_rbMarried);
                }
            }

            if (response.getData().get(0).getHave_child() != null) {
                if (response.getData().get(0).getHave_child().equalsIgnoreCase("1")) {
                    mRgHaveChild.check(R.id.profile_rbNo);
                } else {
                    mRgHaveChild.check(R.id.profile_rbYes);
                }
            }
//    mSpnrProfession.getAccessibilityClassName()


            if (response.getData().get(0).getAnnual_family_income() != null) {
                if (response.getData().get(0).getAnnual_family_income().equalsIgnoreCase("Less Then 5L")) {
                    mRgAnnualIncome.check(R.id.profile_rbLessFive);
                } else if (response.getData().get(0).getAnnual_family_income().equalsIgnoreCase("5-10L")) {
                    mRgAnnualIncome.check(R.id.profile_rbLessTen);
                } else if (response.getData().get(0).getAnnual_family_income().equalsIgnoreCase("10-20L")) {
                    mRgAnnualIncome.check(R.id.profile_rbLessTwenty);
                } else if (response.getData().get(0).getAnnual_family_income().equalsIgnoreCase("20-50L")) {
                    mRgAnnualIncome.check(R.id.profile_rbLessFifty);
                } else if (response.getData().get(0).getAnnual_family_income().equalsIgnoreCase("Greater Then 50L")) {
                    mRgAnnualIncome.check(R.id.profile_rbGreaterFifty);
                }
            }


            if (response.getData().get(0).getMost_active_facebook() != null) {
                if (response.getData().get(0).getMost_active_facebook().equalsIgnoreCase("1")) {
                    mRgFacebook.check(R.id.profile_rbFbSomeTime);
                } else if (response.getData().get(0).getMost_active_facebook().equalsIgnoreCase("2")) {
                    mRgFacebook.check(R.id.profile_rbFbFrequently);
                } else if (response.getData().get(0).getMost_active_facebook().equalsIgnoreCase("3")) {
                    mRgFacebook.check(R.id.profile_rbFbNotActive);
                }
            }


            if (response.getData().get(0).getMost_active_instagram() != null) {
                if (response.getData().get(0).getMost_active_instagram().equalsIgnoreCase("1")) {
                    mRgInstagram.check(R.id.profile_rbIgSomeTime);
                } else if (response.getData().get(0).getMost_active_instagram().equalsIgnoreCase("2")) {
                    mRgInstagram.check(R.id.profile_rbIgFrequently);
                } else if (response.getData().get(0).getMost_active_instagram().equalsIgnoreCase("3")) {
                    mRgInstagram.check(R.id.profile_rbIgNotActive);
                }
            }


            if (response.getData().get(0).getMost_active_youtube() != null) {
                if (response.getData().get(0).getMost_active_youtube().equalsIgnoreCase("1")) {
                    mRgYoutube.check(R.id.profile_rbYtSomeTime);
                } else if (response.getData().get(0).getMost_active_youtube().equalsIgnoreCase("2")) {
                    mRgYoutube.check(R.id.profile_rbYtFrequently);
                } else if (response.getData().get(0).getMost_active_youtube().equalsIgnoreCase("3")) {
                    mRgYoutube.check(R.id.profile_rbYtNotActive);
                }
            }


            if (response.getData().get(0).getMost_active_whatsapp() != null) {
                if (response.getData().get(0).getMost_active_whatsapp().equalsIgnoreCase("1")) {
                    mRgWhatsapp.check(R.id.profile_rbWaSomeTime);
                } else if (response.getData().get(0).getMost_active_whatsapp().equalsIgnoreCase("2")) {
                    mRgWhatsapp.check(R.id.profile_rbWaFrequently);
                } else if (response.getData().get(0).getMost_active_whatsapp().equalsIgnoreCase("3")) {
                    mRgWhatsapp.check(R.id.profile_rbWaNotActive);
                }
            }


            if (response.getData().get(0).getMost_active_travel_site() != null) {
                if (response.getData().get(0).getMost_active_travel_site().equalsIgnoreCase("1")) {
                    mRgTravel.check(R.id.profile_rbTsSomeTime);
                } else if (response.getData().get(0).getMost_active_travel_site().equalsIgnoreCase("2")) {
                    mRgTravel.check(R.id.profile_rbTsFrequently);
                } else if (response.getData().get(0).getMost_active_travel_site().equalsIgnoreCase("3")) {
                    mRgTravel.check(R.id.profile_rbTsNotActive);
                }
            }


            if (response.getData().get(0).getMost_active_restaurant_site() != null) {
                if (response.getData().get(0).getMost_active_restaurant_site().equalsIgnoreCase("1")) {
                    mRgRestaurant.check(R.id.profile_rbRsSomeTime);
                } else if (response.getData().get(0).getMost_active_restaurant_site().equalsIgnoreCase("2")) {
                    mRgRestaurant.check(R.id.profile_rbRsFrequently);
                } else if (response.getData().get(0).getMost_active_restaurant_site().equalsIgnoreCase("3")) {
                    mRgRestaurant.check(R.id.profile_rbRsNotActive);
                }
            }
        } else {
            if (!TextUtils.isEmpty(response.getErrors().getMessage()))
                AlertUtils.showSnackBar(mRlProfile, response.getErrors().getMessage());
        }

    }

    @Override
    public void updateProfileReady(RestResponse response) {
        if (response.getStatus().equalsIgnoreCase("true")) {
            AppPreference.getInstance().setmUserAccessToken(response.getToken());
            AppPreference.getInstance().setmIsProfileComplete(response.getData().get(0).getIs_profile_complete());
            AppPreference.getInstance().setmIsPhoneVerified(response.getData().get(0).getIs_varified());
            AlertUtils.showSnackBarGreen(mRlProfile, response.getMessage());

            if(response.getData().get(0).getIs_varified() != null && response.getData().get(0).getIs_varified().equalsIgnoreCase("0")){
                Intent intent = new Intent(ProfileActivity.this, PhoneVerificationActivity.class);
                startActivity(intent);
                finish();
            }
            else {
                Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

        } else {

                if(response.getErrors().getPhone() != null && response.getErrors().getPhone().length() > 1){
                    AlertUtils.showSnackBar(mRlProfile, response.getErrors().getPhone());
                }
                else{
                    AlertUtils.showSnackBar(mRlProfile, response.getMessage());
                }}


    }

    private boolean validateEmail() {
        String email = mEdtEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            AlertUtils.showSnackBar(mRlProfile, "Please Enter Valid Email Address ");
            requestFocus(mEdtEmail);
            return false;
        } else {

        }
        return true;
    }

    /*  private boolean validatePassword() {
          String password = mEdtPassword.getText().toString().trim();

          if (password.isEmpty() || !isValidPassword(password)) {
              AlertUtils.showSnackBar(mRlProfile, "Please Enter Valid Password");
              requestFocus(mEdtPassword);
              return false;
          } else {

          }
          return true;
      }
  */
    private boolean validatePhone() {
        String phone = mEdtPhone.getText().toString().trim();

        if (phone.isEmpty() || !isValidPhone(phone)) {
            AlertUtils.showSnackBar(mRlProfile, "Please Enter Valid Phone Number");
            requestFocus(mEdtPhone);
            return false;
        } else {

        }
        return true;
    }

    private boolean validateName() {
        String name = mEdtName.getText().toString().trim();

        if (name.isEmpty() || !isValidName(name)) {
            AlertUtils.showSnackBar(mRlProfile, "Please Enter Valid Name");
            requestFocus(mEdtName);
            return false;
        } else {

        }
        return true;
    }

   /* private boolean validateAddress() {
        String address = mEdtAddress.getText().toString().trim();

        if (address.isEmpty()) {
            AlertUtils.showSnackBar(mRlProfile, "Please Enter Address");
            requestFocus(mEdtAddress);
            return false;
        } else {

        }
        return true;
    }*/

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 1;
    }

    private static boolean isValidPhone(String phone) {
        return !TextUtils.isEmpty(phone) && (phone.length() > 1) && (phone.length() <= 10);
    }

    private static boolean isValidName(String name) {
        return !TextUtils.isEmpty(name) && (name.length() > 4);
    }

    private static boolean isValidAddress(String address) {
        return !TextUtils.isEmpty(address);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        mProfession = parent.getItemAtPosition(position).toString();


    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}


