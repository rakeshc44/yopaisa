package com.yopaisa.feature.notification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RelativeLayout;

import com.yopaisa.Model.Data;
import com.yopaisa.Presenter.NotificationPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.DeviceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rakesh on 8/13/2018.
 * Project K.
 */

public class NotificationActivity extends AppCompatActivity implements NotificationPresenter.NotificationListPresenterListener {

    private NotificationPresenter notificationPresenter;
    private List<Data> rateList = new ArrayList<>();
    private RecyclerView recyclerView;
    private NotificationAdapter notificationAdapter;
    private RelativeLayout mRlRateList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_list);

        mRlRateList = findViewById(R.id.ratelist_Rl);
        recyclerView = findViewById(R.id.recycler_view);
        notificationPresenter = new NotificationPresenter(this, this);
        if (DeviceUtils.isInternetOn1(NotificationActivity.this, mRlRateList)) {
            notificationPresenter.getRateList();
        }

    }



    @Override
    public void notificationListReady(List<Data> users) {
        notificationAdapter = new NotificationAdapter(users);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(notificationAdapter);
        notificationAdapter.notifyDataSetChanged();
    }
}
