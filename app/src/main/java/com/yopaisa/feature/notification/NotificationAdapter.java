package com.yopaisa.feature.notification;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yopaisa.Model.Data;
import com.yopaisa.R;

import java.util.List;

/**
 * Created by rakesh on 8/13/2018.
 * Project K.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private List<Data> rateList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtName, mTxtRate;

        public MyViewHolder(View view) {
            super(view);
            mTxtName = view.findViewById(R.id.item_list_TxtTitle);
            mTxtRate = view.findViewById(R.id.item_list_TxtRate);

        }
    }


    public NotificationAdapter(List<Data> rateList) {
        this.rateList = rateList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Data rateLst = rateList.get(position);
        holder.mTxtName.setText(rateLst.getName());
        holder.mTxtRate.setText("Rs " + rateLst.getPrice() + "/" + rateLst.getUnit());

    }

    @Override
    public int getItemCount() {
        return rateList.size();
    }
}
