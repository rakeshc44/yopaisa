package com.yopaisa.feature.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yopaisa.Model.RestResponse;
import com.yopaisa.Presenter.SignUpPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.AlertUtils;
import com.yopaisa.Utils.DeviceUtils;
import com.yopaisa.feature.phoneverify.PhoneVerificationActivity;
import com.yopaisa.feature.welcome.WelcomeActivity;


/**
 * Created by RakeshD on 1/3/2018.
 */

public class SignupActivity extends AppCompatActivity implements SignUpPresenter.SignUpPresenterListener {

    private SignUpPresenter signUpPresenter;
    private String mName, mEmail, mPassword, mPhone;
    private TextInputEditText mEdtName, mEditEmail, mEdtPassword, mEdtPhone, mEdtConfirmPassword, mEdtReferalCode;
    private Button mBtnLogin;
    private RelativeLayout mRlSignin;
    private TextView mTxtTOU;
    String fbTouAndPrivacyPolicyString;
    private String mReferalCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mTxtTOU = findViewById(R.id.signup_txtTOU);
        mEdtName = findViewById(R.id.signup_EdtName);
        mEditEmail = findViewById(R.id.signup_EdtEmail);
        mEdtPassword = findViewById(R.id.signup_EdtPassword);
        mEdtConfirmPassword = findViewById(R.id.signup_EdtConfirmPassword);

        mEdtReferalCode = findViewById(R.id.signup_EdtReferCode);
        mEdtPhone = findViewById(R.id.signup_EdtPhone);
        mBtnLogin = findViewById(R.id.signup_BtnSignup);
        mRlSignin = findViewById(R.id.signin_rl);

        signUpPresenter = new SignUpPresenter(this, this);


        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validateName()) {
                    return;
                }
                if (!validateEmail()) {
                    return;
                }
                if (!validatePassword()) {
                    return;
                }
                if (!validatePhone()) {
                    return;
                }

                if (DeviceUtils.isInternetOn1(SignupActivity.this, mRlSignin)) {
                    mName = mEdtName.getText().toString();
                    mEmail = mEditEmail.getText().toString();
                    mPassword = mEdtPassword.getText().toString();
                    mPhone = mEdtPhone.getText().toString();
                    mReferalCode = mEdtReferalCode.getText().toString();
                    System.out.println(mPassword + ">>>" + mEmail + ">>" + mPhone);

                    signUpPresenter.getSignUp(mName, mEmail, mPassword, mPhone, mReferalCode);
                }
            }
        });

        fbTouAndPrivacyPolicyString = String.format(getString(R.string.str_u_login_agree_privacy_policy),
                getString(R.string.str_terms_of_use), getString(R.string.str_privacy_policy));

        /*createLink(mTxtTOU, fbTouAndPrivacyPolicyString,
                new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Intent intent = new Intent(SignupActivity.this, TermConditionActivity.class);
                        intent.putExtra("TERMSANDCONDITION","TERMSANDCONDITION");
                        startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        // this is where you set link color, underline, typeface etc.
                        int linkColor = ContextCompat.getColor(SignupActivity.this, R.color.colorPrimary);
                        ds.setColor(linkColor);
                        ds.setUnderlineText(true);
                    }
                },
                new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Intent intent = new Intent(SignupActivity.this, TermConditionActivity.class);
                        intent.putExtra("TERMSANDCONDITION","PRIVACYPOLICY");
                        startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        // this is where you set link color, underline, typeface etc.
                        int linkColor = ContextCompat.getColor(SignupActivity.this, R.color.colorPrimary);
                        ds.setColor(linkColor);
                        ds.setUnderlineText(true);
                    }
                }
        );*/
    }

    @Override
    public void signupReady(RestResponse response) {
        System.out.println(">>>>"+"success");
        if (response.getStatus().equalsIgnoreCase("true")) {
            System.out.println(">>>>"+"success1");
            Intent intent = new Intent(SignupActivity.this, PhoneVerificationActivity.class);
            startActivity(intent);
            finish();
        } else {
            if(response.getErrors().getEmail() != null && response.getErrors().getEmail().length() > 0) {
                AlertUtils.showSnackBar(mRlSignin, response.getErrors().getEmail());
            }
            else{

                    AlertUtils.showSnackBar(mRlSignin, response.getErrors().getPhone());

            }
        }


    }

    private boolean validateEmail() {
        String email = mEditEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            AlertUtils.showSnackBar(mRlSignin, "Please Enter Valid Email Address ");
            requestFocus(mEditEmail);
            return false;
        } else {

        }
        return true;
    }

    private boolean validatePassword() {
        String password = mEdtPassword.getText().toString().trim();

        if (password.isEmpty() || !isValidPassword(password)) {
            AlertUtils.showSnackBar(mRlSignin, "Please Enter Valid Password");
            requestFocus(mEdtPassword);
            return false;
        } else {

        }
        return true;
    }

    private boolean validatePhone() {
        String phone = mEdtPhone.getText().toString().trim();

        if (phone.isEmpty() || !isValidPhone(phone)) {
            AlertUtils.showSnackBar(mRlSignin, "Please Enter Valid Phone Number");
            requestFocus(mEdtPhone);
            return false;
        } else {

        }
        return true;
    }

    private boolean validateName() {
        String name = mEdtName.getText().toString().trim();

        if (name.isEmpty() || !isValidName(name)) {
            AlertUtils.showSnackBar(mRlSignin, "Please Enter Valid Name");
            requestFocus(mEdtName);
            return false;
        } else {

        }
        return true;
    }
    private boolean validateReferalCode() {
        String referalCode = mEdtReferalCode.getText().toString().trim();

        if (referalCode.isEmpty() || !isValidReferalCode(referalCode)) {
            AlertUtils.showSnackBar(mRlSignin, "Please Enter Valid Referal Code");
            requestFocus(mEdtReferalCode);
            return false;
        } else {

        }
        return true;
    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 1;
    }

    private static boolean isValidPhone(String phone) {
        return !TextUtils.isEmpty(phone) && (phone.length() > 1) && (phone.length() <= 10);
    }

    private static boolean isValidName(String name) {
        return !TextUtils.isEmpty(name) && (name.length() > 4);
    }
    private static boolean isValidReferalCode(String referalCode) {
        return !TextUtils.isEmpty(referalCode) && (referalCode.length() > 4);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    private void createLink(TextView targetTextView, String completeString, ClickableSpan clickableAction1,
                            ClickableSpan clickableAction2) {

        SpannableString spannableString = new SpannableString(completeString);

        spannableString.setSpan(clickableAction1, 41, 54,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(clickableAction2, 58, 73,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        targetTextView.setText(spannableString);
        targetTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

}

