package com.yopaisa.feature.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.yopaisa.R;
import com.yopaisa.feature.login.LoginActivity;
import com.yopaisa.feature.signup.SignupActivity;


/**
 * Created by rakesh on 8/6/2018.
 * Project K.
 */

public class WelcomeActivity extends AppCompatActivity {

        private Button mBtnLogin, mBtnSignUp;
        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_welcome);
            mBtnLogin = findViewById(R.id.welcome_btnLogin);
            mBtnSignUp = findViewById(R.id.welcome_btnRegister);
            mBtnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(WelcomeActivity.this,LoginActivity.class);
                    startActivity(intent);

                }
            });
            mBtnSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(WelcomeActivity.this,SignupActivity.class);
                    startActivity(intent);

                }
            });
        }
    }

