package com.yopaisa.feature.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yopaisa.MainActivity;
import com.yopaisa.Model.RestResponse;
import com.yopaisa.Presenter.LoginPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.AlertUtils;
import com.yopaisa.Utils.DeviceUtils;
import com.yopaisa.data.Prefs.AppPreference;
import com.yopaisa.feature.phoneverify.PhoneVerificationActivity;
import com.yopaisa.feature.profile.ProfileActivity;


/**
 * Created by RakeshD on 1/3/2018.
 */

public class LoginActivity extends AppCompatActivity implements LoginPresenter.LoginPresenterListener {

    private LoginPresenter loginPresenter;
    private String mEmail, mPassword;
    private EditText mEditEmail, mEdtPassword;
    private Button mBtnLogin;
    private TextView mTxtTOU, mTxtForgotPassword;
    private RelativeLayout mRlLogin;
    String fbTouAndPrivacyPolicyString;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEditEmail = findViewById(R.id.login_EdtEmail);
        mEdtPassword = findViewById(R.id.login_EdtPassword);
        mBtnLogin = findViewById(R.id.login_BtnLogin);
        mTxtTOU = findViewById(R.id.login_txtTOU);
        mTxtForgotPassword = findViewById(R.id.login_TxtForgotPassword);
        mRlLogin = findViewById(R.id.login_rlLayout);


        loginPresenter = new LoginPresenter(this, this);
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateEmail()) {
                    return;
                }
                if (!validatePassword()) {
                    return;
                } else if (DeviceUtils.isInternetOn1(LoginActivity.this, mRlLogin)) {
                    mEmail = mEditEmail.getText().toString();
                    mPassword = mEdtPassword.getText().toString();
                    System.out.println(mPassword + ">>>" + mEmail);
                    loginPresenter.getLogin(mEmail, mPassword);
                }

            }
        });
        mTxtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ForgotPasswordDialog forgotPasswordDialog = new ForgotPasswordDialog(LoginActivity.this, mRlLogin);
                forgotPasswordDialog.show();
            }
        });
        fbTouAndPrivacyPolicyString = String.format(getString(R.string.str_u_login_agree_privacy_policy),
                getString(R.string.str_terms_of_use), getString(R.string.str_privacy_policy));

        /*createLink(mTxtTOU, fbTouAndPrivacyPolicyString,
                new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        Intent intent = new Intent(LoginActivity.this, TermConditionActivity.class);
                        intent.putExtra("TERMSANDCONDITION","TERMSANDCONDITION");
                        startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        // this is where you set link color, underline, typeface etc.
                        int linkColor = ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary);
                        ds.setColor(linkColor);
                        ds.setUnderlineText(true);
                    }
                },
                new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                                Intent intent = new Intent(LoginActivity.this, TermConditionActivity.class);
                                intent.putExtra("TERMSANDCONDITION","PRIVACYPOLICY");
                                startActivity(intent);
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        // this is where you set link color, underline, typeface etc.
                        int linkColor = ContextCompat.getColor(LoginActivity.this, R.color.colorPrimary);
                        ds.setColor(linkColor);
                        ds.setUnderlineText(true);
                    }
                }
        );*/

    }

    @Override
    public void loginReady(RestResponse response) {

        if (response.getStatus().equalsIgnoreCase("true")) {
            AppPreference.getInstance().setmUserAccessToken(response.getToken());
            AppPreference.getInstance().setmUserName(response.getData().get(0).getName());
            AppPreference.getInstance().setmUserPhoneNumber(response.getData().get(0).getPhone());
            AppPreference.getInstance().setmUserMailId(response.getData().get(0).getEmail());
            if(response.getData().get(0).getAddress() != null)
            AppPreference.getInstance().setmUserAddress(response.getData().get(0).getAddress());

            if(response.getData().get(0).getPassword() != null)
                AppPreference.getInstance().setmUserPassword(response.getData().get(0).getPassword());

            if (response.getData().get(0).getIs_varified().equalsIgnoreCase("1")) {
                if (response.getData().get(0).getIs_profile_complete().equalsIgnoreCase("1")) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
                    startActivity(intent);
                    finish();
                }

            } else {
                Intent intent = new Intent(LoginActivity.this, PhoneVerificationActivity.class);
                startActivity(intent);
                finish();
            }

        } else {  AlertUtils.showSnackBar(mRlLogin, response.getMessage());

        }
//        Toast.makeText(LoginActivity.this, "success" , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginError() {
        AlertUtils.showSnackBar(mRlLogin, "Please Enter Valid Email or Password");
    }

    private void createLink(TextView targetTextView, String completeString, ClickableSpan clickableAction1,
                            ClickableSpan clickableAction2) {

        SpannableString spannableString = new SpannableString(completeString);

        spannableString.setSpan(clickableAction1, 41, 54,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(clickableAction2, 58, 73,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        targetTextView.setText(spannableString);
        targetTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private boolean validateEmail() {
        String email = mEditEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            AlertUtils.showSnackBar(mRlLogin, "Please Enter Valid Email Address ");
            requestFocus(mEditEmail);
            return false;
        } else {

        }
        return true;
    }

    private boolean validatePassword() {
        String password = mEdtPassword.getText().toString().trim();

        if (password.isEmpty() || !isValidPassword(password)) {
            AlertUtils.showSnackBar(mRlLogin, "Please Enter Valid Password");
            requestFocus(mEdtPassword);
            return false;
        } else {

        }
        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private static boolean isValidPassword(String password) {
        return !TextUtils.isEmpty(password) && password.length() > 1;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
