package com.yopaisa.feature.login;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.yopaisa.Presenter.ForgotPasswordPresenter;
import com.yopaisa.R;
import com.yopaisa.Utils.AlertUtils;
import com.yopaisa.Utils.DeviceUtils;
import com.yopaisa.response.ForgotPasswordResponse;

import java.util.List;

/**
 * Created by rakesh on 6/19/2018.
 * Project K.
 */

public class ForgotPasswordDialog extends Dialog implements ForgotPasswordPresenter.ForgotPasswordPresenterListener {
    LoginActivity mContext1;
    private View mView;
    private TextView mTxtTitle;
    private EditText mEdtEmail;
    private Button mBtnSend, mBtnCancel;
    private ForgotPasswordPresenter forgotPasswordPresenter;
    private LinearLayout mLnrForgotPassword;
    private RelativeLayout mRlLogin;


    public ForgotPasswordDialog(@NonNull Context context) {
        super(context);
    }

    public ForgotPasswordDialog(LoginActivity context, RelativeLayout mRlLogin) {
        super(context, R.style.AdvanceDialogTheme);
        this.mContext1 = context;
        this.mRlLogin =mRlLogin;
        init();
    }

    private void init() {
        setContentView(R.layout.dialog_forgot_password);
        setCancelable(true);

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.dimAmount = 0.9f;
        getWindow().setAttributes(lp);
        setCanceledOnTouchOutside(true);
        mView = findViewById(R.id.view);
        mTxtTitle = findViewById(R.id.dialogForgotPassword_txtTitle);
        mEdtEmail = findViewById(R.id.dialogForgotPassword_edtEmail);
        mBtnSend = findViewById(R.id.dialogForgotPassword_btnSend);
        mBtnCancel = findViewById(R.id.dialogForgotPassword_btnCancel);
        mLnrForgotPassword = findViewById(R.id.forgotpassword_lnr);

        forgotPasswordPresenter = new ForgotPasswordPresenter(this, mContext1);
        mBtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validateEmail()) {
                    return;
                }
                else if (DeviceUtils.isInternetOn1(mContext1,mLnrForgotPassword)) {
                    forgotPasswordPresenter.getForgotPassword(mEdtEmail.getText().toString());
                }
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void forgotPasswordReady(ForgotPasswordResponse response) {

        if (response.getStatus().equalsIgnoreCase("true")) {

         /*   PasswordDialog passwordDialog = new PasswordDialog(mContext1,mRlLogin);
            passwordDialog.show();*/
            dismiss();
            //AlertUtils.showSnackBarGreen(mRlLogin, response.getMessage());

        }
        else {
            dismiss();
            AlertUtils.showSnackBar(mRlLogin, response.getErrors().getEmail());
        }

    }

    private boolean validateEmail() {
        String email = mEdtEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            AlertUtils.showSnackBar(mLnrForgotPassword, "Please Enter Valid Email Address ");
            requestFocus(mEdtEmail);
            return false;
        } else {

        }
        return true;
    }
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

}
