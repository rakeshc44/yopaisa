package com.yopaisa.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by RakeshD on 11/24/2017.
 */

public class Users
{

    @SerializedName("userId")
    @Expose
    private Integer userId;

    private List<String> items;

    private String name;

    private String image;

    public List<String> getItems ()
    {
        return items;
    }

    public void setItems (List<String> items)
    {
        this.items = items;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getImage ()
    {
        return image;
    }

    public void setImage (String image)
    {
        this.image = image;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [items = "+items+", name = "+name+", image = "+image+"]";
    }
}
