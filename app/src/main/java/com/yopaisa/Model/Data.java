package com.yopaisa.Model;

import java.io.Serializable;

/**
 * Created by RakeshD on 11/24/2017.
 */

public class Data implements Serializable {

    private String phone;



    private String email;
    private String gender;
    private String image;
    private String type;

    private String password;

    private String last_login;

    private String unit;

    private String price;

    private String landmark;

    private String address;

    private String items;

    private String longitude;

    private String user_id;

    private String latitude;

    private String pickup_time_from;

    private String pickup_date;

    private String pickup_time_to;

    private String id;

    private String page_url;

    private String description;

    private String name;

    private String added_on;

    private String add_on;

    private String status;

    private String answer;

    private String question;

    private String have_child;



    private String zip_code;

    private String profession;



    private String most_active_facebook;



    private String city;



    private String most_active_whatsapp;

    private String is_varified;



    private String marital_status;

    private String otp;

    private String most_active_youtube;

    private String parent_id;



    private String annual_family_income;



    private String most_active_instagram;

    private String most_active_restaurant_site;



    private String is_profile_complete;



    private String most_active_travel_site;

    public String getHave_child ()
    {
        return have_child;
    }

    public void setHave_child (String have_child)
    {
        this.have_child = have_child;
    }



    public String getZip_code ()
    {
        return zip_code;
    }

    public void setZip_code (String zip_code)
    {
        this.zip_code = zip_code;
    }

    public String getProfession ()
    {
        return profession;
    }

    public void setProfession (String profession)
    {
        this.profession = profession;
    }



    public String getMost_active_facebook ()
    {
        return most_active_facebook;
    }

    public void setMost_active_facebook (String most_active_facebook)
    {
        this.most_active_facebook = most_active_facebook;
    }



    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }





    public String getMost_active_whatsapp ()
    {
        return most_active_whatsapp;
    }

    public void setMost_active_whatsapp (String most_active_whatsapp)
    {
        this.most_active_whatsapp = most_active_whatsapp;
    }

    public String getIs_varified ()
    {
        return is_varified;
    }

    public void setIs_varified (String is_varified)
    {
        this.is_varified = is_varified;
    }



    public String getMarital_status ()
    {
        return marital_status;
    }

    public void setMarital_status (String marital_status)
    {
        this.marital_status = marital_status;
    }

    public String getOtp ()
    {
        return otp;
    }

    public void setOtp (String otp)
    {
        this.otp = otp;
    }

    public String getMost_active_youtube ()
    {
        return most_active_youtube;
    }

    public void setMost_active_youtube (String most_active_youtube)
    {
        this.most_active_youtube = most_active_youtube;
    }

    public String getParent_id ()
    {
        return parent_id;
    }

    public void setParent_id (String parent_id)
    {
        this.parent_id = parent_id;
    }



    public String getAnnual_family_income ()
    {
        return annual_family_income;
    }

    public void setAnnual_family_income (String annual_family_income)
    {
        this.annual_family_income = annual_family_income;
    }



    public String getMost_active_instagram ()
    {
        return most_active_instagram;
    }

    public void setMost_active_instagram (String most_active_instagram)
    {
        this.most_active_instagram = most_active_instagram;
    }

    public String getMost_active_restaurant_site ()
    {
        return most_active_restaurant_site;
    }

    public void setMost_active_restaurant_site (String most_active_restaurant_site)
    {
        this.most_active_restaurant_site = most_active_restaurant_site;
    }



    public String getIs_profile_complete ()
    {
        return is_profile_complete;
    }

    public void setIs_profile_complete (String is_profile_complete)
    {
        this.is_profile_complete = is_profile_complete;
    }




    public String getMost_active_travel_site ()
    {
        return most_active_travel_site;
    }

    public void setMost_active_travel_site (String most_active_travel_site)
    {
        this.most_active_travel_site = most_active_travel_site;
    }

    public String getAdd_on ()
    {
        return add_on;
    }

    public void setAdd_on (String add_on)
    {
        this.add_on = add_on;
    }



    public String getAnswer ()
    {
        return answer;
    }

    public void setAnswer (String answer)
    {
        this.answer = answer;
    }

    public String getQuestion ()
    {
        return question;
    }

    public void setQuestion (String question)
    {
        this.question = question;
    }


    public String getPage_url ()
    {
        return page_url;
    }

    public void setPage_url (String page_url)
    {
        this.page_url = page_url;
    }


    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }



    public String getAdded_on ()
    {
        return added_on;
    }

    public void setAdded_on (String added_on)
    {
        this.added_on = added_on;
    }





    public String getLandmark ()
    {
        return landmark;
    }

    public void setLandmark (String landmark)
    {
        this.landmark = landmark;
    }



    public String getItems ()
    {
        return items;
    }

    public void setItems (String items)
    {
        this.items = items;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }



    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getPickup_time_from() {
        return pickup_time_from;
    }

    public void setPickup_time_from(String pickup_time_from) {
        this.pickup_time_from = pickup_time_from;
    }

    public String getPickup_date() {
        return pickup_date;
    }

    public void setPickup_date(String pickup_date) {
        this.pickup_date = pickup_date;
    }

    public String getPickup_time_to() {
        return pickup_time_to;
    }

    public void setPickup_time_to(String pickup_time_to) {
        this.pickup_time_to = pickup_time_to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLast_login() {
        return last_login;
    }

    public void setLast_login(String last_login) {
        this.last_login = last_login;
    }

    @Override
    public String toString() {
        return "ClassPojo [phone = " + phone + ", address = " + address + ", add_on = " + add_on + ", status = " + status + ", email = " + email + ", name = " + name + ", gender = " + gender + ", image = " + image + ", user_id = " + user_id + ", type = " + type + ", password = " + password + ", last_login = " + last_login + "]";
    }


}