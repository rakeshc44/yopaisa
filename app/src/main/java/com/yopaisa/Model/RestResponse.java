package com.yopaisa.Model;

import java.util.ArrayList;

/**
 * Created by jean on 29/07/16.
 */

public class RestResponse {
    private String message;

    private String token;

    private String status;

    private ArrayList<Data> data;

    private Errors errors;

    private String id;

    private String unit;

    private String price;

    private String add_on;
    private String api_version;

    private String total_records;

    private String name;


    public  Errors getErrors ()
    {
        return errors;
    }

    public void setErrors ( Errors errors)
    {
        this.errors = errors;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAdd_on() {
        return add_on;
    }

    public void setAdd_on(String add_on) {
        this.add_on = add_on;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }
    public String getApi_version ()
    {
        return api_version;
    }

    public void setApi_version (String api_version)
    {
        this.api_version = api_version;
    }

    public String getTotal_records ()
    {
        return total_records;
    }

    public void setTotal_records (String total_records)
    {
        this.total_records = total_records;
    }
    @Override
    public String toString() {
        return "ClassPojo [message = " + message + ", name = " + getData().get(0).getName() + ", errors = " + errors+ ", status = " + status + ", data = " + data + "]";
    }

}