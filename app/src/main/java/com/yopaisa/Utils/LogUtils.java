package com.yopaisa.Utils;

import android.util.Log;


/**
 * Created by Rakesh Dhaundiyal on 1/9/2107.
 * @author This class is used for adding log in-line. 
 * Need to change "mIsDebug" value "false" when deploy.
 *
 */
public class LogUtils {
	
	private static boolean mIsDebug = true;
	
	public static void info(Object object){
		if(mIsDebug){
			Log.i("Kabhadi_Log : ", ""+object);
		}
	}
	
	public static void info(String tag, Object object){
		if(mIsDebug){
			Log.i("Kabhadi_Log : " + tag, ""+object);
		}
	}
	
	public static void debug(Object object){
		if(mIsDebug){
			Log.d("Kabhadi_Log : ", ""+object);
		}
	}
	
	public static void debug(String tag, Object object){
		if(mIsDebug){
			Log.d("Kabhadi_Log : " + tag, ""+object);
		}
	}

	public static void debug(String tag, Object object, Throwable exception){
		if(mIsDebug){
			Log.d("Kabhadi_Log : " + tag, ""+object,exception);
		}
	}

	public static void error(Object object){
		if(mIsDebug){
			Log.e("Kabhadi_Log : ", ""+object);
			if(object instanceof Exception){
				((Exception) object).printStackTrace();
			}
		}
	}
	
	public static void error(String tag, Object object){
		if(mIsDebug){
			Log.e("Kabhadi_Log : " + tag, ""+object);
			if(object instanceof Exception){
				((Exception) object).printStackTrace();
			}
		}
	}
	
	public static void print(Object object){
		if(mIsDebug){
			System.out.println("Kabhadi_Log : " + object);
		}
	}
}
