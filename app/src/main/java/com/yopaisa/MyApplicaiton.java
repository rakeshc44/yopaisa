package com.yopaisa;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by RakeshD on 11/24/2017.
 */

public class MyApplicaiton extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);

        AppContext context = AppContext.getInstance();
        context.setContext(this);
    }
}
