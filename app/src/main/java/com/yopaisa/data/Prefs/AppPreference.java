package com.yopaisa.data.Prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.yopaisa.AppContext;


/**
 * Created by Rakesh Dhaundiyal on 1/9/2017.
 * {@link AppPreference class is used to implement SharedPreference}
 */

public class AppPreference {

    private String TAG = this.getClass().getSimpleName();
    private String TAG_LOCAL = "local_notification";
    private SharedPreferences mPrefs;
    private SharedPreferences mPrefslocal;
    private SharedPreferences.Editor mPrefsEditor;
    private SharedPreferences.Editor mPrefsEditorlocal;


    private final String mUserAccessToken = "mUserAccessToken";
    private final String mUserName = "mUserName";
    private final String mUserPhoneNumber = "mUserPhoneNumber";
    private final String mUserMailId = "mUserMailId";
    private final String mUserProfilePic = "mUserProfilePic";
    private final String mUserPassword = "mUserPassword";
    private final String mUserAddress = "mUserAddress";
    private final String mIsPhoneVerified = "mIsPhoneVerified";
    private final String mIsProfileComplete = "mIsProfileComplete";



    private static AppPreference INSTANCE;


    public static AppPreference getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new AppPreference(AppContext.getInstance().getContext());
        }
        return INSTANCE;
    }

    private AppPreference(Context context) {
        this.mPrefs = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        this.mPrefslocal = context.getSharedPreferences(TAG_LOCAL, Context.MODE_PRIVATE);
        this.mPrefsEditor = mPrefs.edit();
        this.mPrefsEditorlocal = mPrefslocal.edit();
    }




    public void setmUserAccessToken(String value) {
        Log.d("setmUserAccessToken = ", value);
        mPrefsEditor.putString(mUserAccessToken, value);
        mPrefsEditor.commit();
    }

    public String getmUserAccessToken() {
        return mPrefs.getString(mUserAccessToken, "");
    }


    public void setmIsPhoneVerified(String value) {
        Log.d("setmIsPhoneVerified = ", value);
        mPrefsEditor.putString(mIsPhoneVerified, value);
        mPrefsEditor.commit();
    }

    public String getmIsPhoneVerified() {
        return mPrefs.getString(mIsPhoneVerified, "");
    }

    public void setmIsProfileComplete(String value) {
        Log.d("setmIsProfileCompl = ", value);
        mPrefsEditor.putString(mIsProfileComplete, value);
        mPrefsEditor.commit();
    }

    public String getmIsProfileComplete() {
        return mPrefs.getString(mIsProfileComplete, "");
    }




    public void setmUserName(String value) {
        Log.d("setmUserName = ", value);
        mPrefsEditor.putString(mUserName, value);
        mPrefsEditor.commit();
    }

    public String getmUserName() {
        return mPrefs.getString(mUserName, "");
    }


    public void setmUserPassword(String value) {
        Log.d("setmUserPassword = ", value);
        mPrefsEditor.putString(mUserPassword, value);
        mPrefsEditor.commit();
    }

    public String getmUserPassword() {
        return mPrefs.getString(mUserPassword, "");
    }

    public void setmUserAddress(String value) {
        Log.d("setmUserAddress = ", value);
        mPrefsEditor.putString(mUserAddress, value);
        mPrefsEditor.commit();
    }

    public String getmUserAddress() {
        return mPrefs.getString(mUserAddress, "");
    }

    public void setmUserPhoneNumber(String value) {
        Log.d("setmUserPhoneNumber = ", value);
        mPrefsEditor.putString(mUserPhoneNumber, value);
        mPrefsEditor.commit();
    }

    public String getmUserPhoneNumber(){
        return mPrefs.getString(mUserPhoneNumber, "");
    }

    public void setmUserMailId(String value) {
        Log.d("setmUserMailId = ", value);
        mPrefsEditor.putString(mUserMailId, value);
        mPrefsEditor.commit();
    }

    public String getmUserMailId() {
        return mPrefs.getString(mUserMailId, "");
    }

    public void setmUserProfilePic(String value) {
        Log.d("setmUserProfilePic = ", value);
        mPrefsEditor.putString(mUserProfilePic, value);
        mPrefsEditor.commit();
    }

    public String getmUserProfilePic() {
        return mPrefs.getString(mUserProfilePic, "");
    }


}