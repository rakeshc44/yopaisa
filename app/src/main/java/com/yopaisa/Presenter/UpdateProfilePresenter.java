package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.UpdateProfileService;
import com.yopaisa.data.Prefs.AppPreference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 6/21/2018.
 * Project K.
 */

public class UpdateProfilePresenter {
    private final Context context;
    private final UpdateProfilePresenterListener mListener;
    private final UpdateProfileService updateProfileService;


    public interface UpdateProfilePresenterListener {
        void updateProfileReady(RestResponse users);
    }

    public UpdateProfilePresenter(UpdateProfilePresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.updateProfileService = new UpdateProfileService();
    }

    public void getUpdateProfile(String mName, String mEmail, String mPhone, String mCity, String mPinCode, String mGender,
                                 String mIsMarried, String mHaveChild, String mProfession, String mAnnualIncome, String mFacebookCount,
                                 String mInstagraCount,
                                 String mYoutubeCount,
                                 String mWhatsappCount,
                                 String mTravelSiteCount,String mRestaurantCount

                                 ) {
        System.out.println(mName+"\n"+mEmail+"\n"+mPhone+"\n"+mCity+"\n"+mPinCode+"\n"+mGender+"\n"+mIsMarried+"\n"+mHaveChild+"\n"+
                mProfession+"\n"+mAnnualIncome+"\n"+mFacebookCount+"\n"+mInstagraCount+"\n"
                +mYoutubeCount+"\n"+mWhatsappCount+"\n"+mTravelSiteCount+"\n"+mRestaurantCount);

        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        updateProfileService
                .getAPI()
                .getResults(String.valueOf(mName), String.valueOf(mEmail), String.valueOf(mPhone), String.valueOf(mCity),
                        String.valueOf(mPinCode), String.valueOf(mGender), String.valueOf(mIsMarried),
                        String.valueOf(mHaveChild), String.valueOf(mProfession),String.valueOf(mAnnualIncome),
                        String.valueOf(mFacebookCount),
                        String.valueOf(mInstagraCount),
                        String.valueOf(mYoutubeCount),
                        String.valueOf(mWhatsappCount),
                        String.valueOf(mTravelSiteCount),
                        String.valueOf(mRestaurantCount), AppPreference.getInstance().getmUserPassword(),AppPreference.getInstance().getmUserAddress())
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        progressDialog.dismiss();


                        RestResponse result = response.body();



                            mListener.updateProfileReady(result);

                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        try {
                            throw new InterruptedException("Errors");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
