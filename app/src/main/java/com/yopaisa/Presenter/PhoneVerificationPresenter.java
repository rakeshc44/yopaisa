package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.yopaisa.Model.Data;
import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.AboutUsService;
import com.yopaisa.Service.PhoneVerificationService;
import com.yopaisa.data.Prefs.AppPreference;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 8/13/2018.
 * Project K.
 */

public class PhoneVerificationPresenter {
    private final Context context;
    private final PhoneVerificationPresenter.PhoneVerificationPresenterListener mListener;
    private final PhoneVerificationService phoneVerificationService;


    public interface PhoneVerificationPresenterListener {
        void otpVerifyReady(RestResponse response);
    }

    public PhoneVerificationPresenter(PhoneVerificationPresenter.PhoneVerificationPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.phoneVerificationService = new PhoneVerificationService();
    }

    public void getOtpVerify(String mOtpCode) {
String mEmail = AppPreference.getInstance().getmUserMailId();
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        phoneVerificationService
                .getAPI()
                .getResults(String.valueOf(mOtpCode),String.valueOf(mEmail))
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        progressDialog.dismiss();

                        RestResponse result = response.body();



                            mListener.otpVerifyReady(result);

                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        try {
                            throw new InterruptedException("Errors");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}

