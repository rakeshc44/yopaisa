package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;


import com.yopaisa.Model.Data;
import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.FaqService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 6/5/2018.
 */

public class FaqPresenter {
    private final Context context;
    private final FaqPresenter.FaqListPresenterListener mListener;
    private final FaqService faqService;


    public interface FaqListPresenterListener {
        void faqListReady(List<Data> users);
    }

    public FaqPresenter(FaqPresenter.FaqListPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.faqService = new FaqService();
    }

    public void getFaqList() {
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        faqService
                .getAPI()
                .getResults()
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        try {
                            progressDialog.dismiss();
                            ArrayList<Data> result = response.body().getData();

                            if(response.body().getStatus().equalsIgnoreCase("true")) {
                                mListener.faqListReady(result);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        try {
                            progressDialog.dismiss();
                            try {
                                throw new InterruptedException("Errors");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
