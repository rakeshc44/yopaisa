package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.ProfileService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 6/5/2018.
 */

public class ProfilePresenter {
    private final Context context;
    private final ProfilePresenterListener mListener;
    private final ProfileService profileService;
    ProgressDialog progressBar;

    public interface ProfilePresenterListener {
        void profileReady(RestResponse users);
    }

    public ProfilePresenter(ProfilePresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.profileService = new ProfileService();
    }

    public void getProfile() {
        if (progressBar != null && progressBar.isShowing()) {
            progressBar.dismiss();
        }
        progressBar = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressBar.show();
        progressBar.setTitle("Loading....");
        profileService
                .getAPI()
                .getResults( )
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        progressBar.dismiss();


                        RestResponse result = response.body();
                            mListener.profileReady(result);


                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            throw new InterruptedException("Errors");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
