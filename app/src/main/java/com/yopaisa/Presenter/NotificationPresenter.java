package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.yopaisa.Model.Data;
import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.NotificationService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 8/13/2018.
 * Project K.
 */

public class NotificationPresenter {
    private final Context context;
    private final NotificationListPresenterListener mListener;
    private final NotificationService notificationService;
    ;

    public interface NotificationListPresenterListener{
        void notificationListReady(List<Data> users);
    }

    public NotificationPresenter(NotificationListPresenterListener listener, Context context){
        this.mListener = listener;
        this.context = context;
        this.notificationService = new NotificationService();
    }

    public void getRateList(){
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        notificationService
                .getAPI()
                .getResults()
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        try {
                            progressDialog.dismiss();
                            ArrayList<Data> result = response.body().getData();
                            if(response.body().getStatus().equalsIgnoreCase("true")) {
                                mListener.notificationListReady(result);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        try {
                            progressDialog.dismiss();
                            try {
                                throw new InterruptedException("Errors");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}

