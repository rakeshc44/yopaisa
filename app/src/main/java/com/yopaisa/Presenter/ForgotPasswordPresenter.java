package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;


import com.yopaisa.R;
import com.yopaisa.Service.ForgotPasswordService;
import com.yopaisa.response.ForgotPasswordResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 6/19/2018.
 * Project K.
 */

public class ForgotPasswordPresenter {
    private final Context context;
    private final ForgotPasswordPresenter.ForgotPasswordPresenterListener mListener;
    private final ForgotPasswordService forgotPasswordService;


    public interface ForgotPasswordPresenterListener{
        void forgotPasswordReady(ForgotPasswordResponse users);
    }

    public ForgotPasswordPresenter(ForgotPasswordPresenter.ForgotPasswordPresenterListener listener, Context context){
        this.mListener = listener;
        this.context = context;
        this.forgotPasswordService = new ForgotPasswordService();
    }

    public void getForgotPassword(String mEmail){
        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        forgotPasswordService
                .getAPI()
                .getResults(mEmail)
                .enqueue(new Callback<ForgotPasswordResponse>() {
                    @Override
                    public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                        try {
                            progressDialog.dismiss();
                            ForgotPasswordResponse result = response.body();

                                mListener.forgotPasswordReady(result);


                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                        try {
                            progressDialog.dismiss();
                            try {
                                throw new InterruptedException("Errors");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
