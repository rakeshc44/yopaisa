package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;


import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.SignUpService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RakeshD on 1/3/2018.
 */

public class SignUpPresenter {
    private final Context context;
    private final SignUpPresenter.SignUpPresenterListener mListener;
    private final SignUpService signUpService;


    public interface SignUpPresenterListener {
        void signupReady(RestResponse users);
    }

    public SignUpPresenter(SignUpPresenter.SignUpPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.signUpService = new SignUpService();
    }

    public void getSignUp(String mName, String mEmail, String mPassword, String mPhone, String mReferalCode) {
        String mAddress = "Janakpuri  rtdr yrd ydy d New Delhi";
        String mType = "12";
        String mCity= "Delhi";
        String mZipCode = "110059";
        System.out.println(mName+"+"+mEmail +"+" + mPassword + "+" + mPhone + "+" + mAddress + "+" + mType);

        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        signUpService
                .getAPI()
                .getResults(String.valueOf(mName), String.valueOf(mEmail), String.valueOf(mPassword), String.valueOf(mPhone), String.valueOf(mAddress),String.valueOf(mType), String.valueOf(mCity), String.valueOf(mZipCode),String.valueOf(mReferalCode))
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        progressDialog.dismiss();

                        System.out.println(">>>>>>"+response.body().getStatus());
                        RestResponse result = response.body();

                        mListener.signupReady(result);

                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        try {
                            throw new InterruptedException("Errors");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
