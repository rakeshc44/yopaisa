package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;


import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.LoginService;
import com.yopaisa.Utils.AlertUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RakeshD on 1/3/2018.
 */

public class LoginPresenter {

    private final Context context;
    private final LoginPresenterListener mListener;
    private final LoginService loginService;


    public interface LoginPresenterListener {
       void loginReady(RestResponse users);
        void loginError();
    }

    public LoginPresenter(LoginPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.loginService = new LoginService();
    }

    public void getLogin(String mEmail, String mPassword) {

        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loging In....");
        loginService
                .getAPI()
                .getResults(String.valueOf(mEmail), String.valueOf(mPassword))
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        progressDialog.dismiss();

                       RestResponse result = response.body();
                        System.out.println(result +">>>1" + response.body());

                            mListener.loginReady(result);
                        progressDialog.cancel();

                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        try {
                            mListener.loginError();
                            throw new InterruptedException("Errors");


                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
