package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;


import com.yopaisa.Model.Data;
import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.AboutUsService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RakeshD on 1/22/2018.
 */

public class AboutUsPresenter {
    private final Context context;
    private final AboutUsPresenter.AboutUsPresenterListener mListener;
    private final AboutUsService aboutUsService;


    public interface AboutUsPresenterListener {
        void aboutUsReady(List<Data> users);
    }

    public AboutUsPresenter(AboutUsPresenter.AboutUsPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.aboutUsService = new AboutUsService();
    }

    public void getAboutUs(String mPageUrl) {

        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        aboutUsService
                .getAPI()
                .getResults(String.valueOf(mPageUrl))
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        progressDialog.dismiss();




                        if(response.body().getStatus().equalsIgnoreCase("true")) {
                            ArrayList<Data> result = response.body().getData();
                            mListener.aboutUsReady(result);
                        }
                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        try {
                            throw new InterruptedException("Errors");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
