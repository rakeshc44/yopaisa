package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;


import com.yopaisa.Model.RestResponse;
import com.yopaisa.R;
import com.yopaisa.Service.TermConditionService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by RakeshD on 1/22/2018.
 */

public class TermConditionPresenter {
    private final Context context;
    private final TermConditionPresenter.TermConditionPresenterListener mListener;
    private final TermConditionService termConditionService;
    ProgressDialog progressBar;

    public interface TermConditionPresenterListener {
        void termConditionReady(RestResponse users);
    }

    public TermConditionPresenter(TermConditionPresenter.TermConditionPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.termConditionService = new TermConditionService();
    }

    public void getTermCondition(String mPageUrl) {

        progressBar = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressBar.show();
        progressBar.setTitle("Loading....");
        termConditionService
                .getAPI()
                .getResults(String.valueOf(mPageUrl))
                .enqueue(new Callback<RestResponse>() {
                    @Override
                    public void onResponse(Call<RestResponse> call, Response<RestResponse> response) {
                        progressBar.dismiss();
                        RestResponse result = response.body();
                        mListener.termConditionReady(result);

                    }

                    @Override
                    public void onFailure(Call<RestResponse> call, Throwable t) {
                        progressBar.dismiss();
                        try {
                            throw new InterruptedException("Errors");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
