package com.yopaisa.Presenter;

import android.app.ProgressDialog;
import android.content.Context;


import com.yopaisa.R;
import com.yopaisa.Service.ContactUsService;
import com.yopaisa.response.ContactUsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 5/30/2018.
 */

public class ContactUsPresenter {
    private final Context context;
    private final ContactUsPresenter.ContactusPresenterListener mListener;
    private final ContactUsService contactUsService;


    public interface ContactusPresenterListener {
        void ContactUsReady(ContactUsResponse data);
    }

    public ContactUsPresenter(ContactUsPresenter.ContactusPresenterListener listener, Context context) {
        this.mListener = listener;
        this.context = context;
        this.contactUsService = new ContactUsService();
    }

    public void getContactUs(String mName, String mEmail, String mPhone, String mMessage) {

        final ProgressDialog progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.show();
        progressDialog.setTitle("Loading....");
        contactUsService
                .getAPI()
                .getResults(String.valueOf(mName), String.valueOf(mEmail), String.valueOf(mPhone), String.valueOf(mMessage))
                .enqueue(new Callback<ContactUsResponse>() {
                    @Override
                    public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {
                        progressDialog.dismiss();
                        ContactUsResponse result = response.body();



                            mListener.ContactUsReady(result);

                    }

                    @Override
                    public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        try {
                            throw new InterruptedException("Errors");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}


