package com.yopaisa.response;


import com.yopaisa.Model.Data;
import com.yopaisa.Model.Errors;

/**
 * Created by rakesh on 6/22/2018.
 * Project K.
 */

public class ForgotPasswordResponse {
    private String message;

    private Errors errors;

    private String token;

    private String status;

    private Data data;


    private String api_version;

    private String total_records;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public Errors getErrors ()
    {
        return errors;
    }

    public void setErrors (Errors errors)
    {
        this.errors = errors;
    }

    public String getToken ()
    {
        return token;
    }

    public void setToken (String token)
    {
        this.token = token;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public String getApi_version ()
    {
        return api_version;
    }

    public void setApi_version (String api_version)
    {
        this.api_version = api_version;
    }

    public String getTotal_records ()
    {
        return total_records;
    }

    public void setTotal_records (String total_records)
    {
        this.total_records = total_records;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", errors = "+errors+", token = "+token+", status = "+status+", data = "+data+", api_version = "+api_version+", total_records = "+total_records+"]";
    }
}
