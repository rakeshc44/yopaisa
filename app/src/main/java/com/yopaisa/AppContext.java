package com.yopaisa;

import android.app.Application;


/**
 * Created by Rakesh Dhaundiyal on 1/9/2107.
 * AppContext to create instance
 */
public class AppContext {
    private static AppContext INSTANCE = null;
    private Application mApplication;

    public static AppContext getInstance() {

        if (INSTANCE == null) INSTANCE = new AppContext();
        return INSTANCE;
    }

    public Application getContext() {
        return this.mApplication;
    }

    public void setContext(Application application) {
        this.mApplication = application;
    }


}
